package experiments;

import java.util.Scanner;

public class Cviko03_Uloha02
{
    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);
        int rok = scanner.nextInt();

        if(rok % 4 != 0)
        {
            System.out.println("Není přestupný");
        }
        else
        {
            if(rok % 100 != 0)
            {
                System.out.println("Je přestupný");
            }
            else
            {
                if(rok % 400 != 0)
                {
                    System.out.println("Není přestupný");
                }
                else
                {
                    System.out.println("Je přestupný");
                }
            }
        }

        // Vypiš, zda je zadaný rok přestupný
    }
}
