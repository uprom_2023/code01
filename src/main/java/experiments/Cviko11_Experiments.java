package experiments;

import java.io.FileNotFoundException;

public class Cviko11_Experiments
{
    public static void main(String[] args) throws FileNotFoundException
    {
        Cviko11_List list = new Cviko11_List();
        Cviko11_Utils.fillExample(list);
        System.out.println(list.get(0));
        System.out.println(list.get(1));
        System.out.println(list.get(2));
        System.out.println(list.join(" "));

        String[] pole1 = new String[100];

        String[] pole2 = {"Ahoj","Hello"};
        String[] pole2b = new String[]{"Ahoj","Hello"};
        //pole2[10];
        //pole2{10};
        //pole2(10);
    }

    public static int lastEvenValue(int[] pole)
    {
        int result = 0;
        for(int i=0; i<pole.length; i++)
        {
            if(pole[i]%2 ==0)
            {
                result = pole[i];
            }
        }
        return result;
    }
}
