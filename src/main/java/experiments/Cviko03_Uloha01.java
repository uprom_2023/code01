package experiments;

import java.util.Scanner;

public class Cviko03_Uloha01
{
    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);
        double hrubaMzda = scanner.nextDouble();
        double danZPrijmu =0;
        if(hrubaMzda < 161296)
        {
            danZPrijmu = hrubaMzda * 0.15;
        }
        else
        {
            danZPrijmu = 161296 * 0.15 + (hrubaMzda - 161296) * 0.23;
        }

        System.out.println(hrubaMzda - danZPrijmu);

        //	Daň z příjmů fyzických osob činí
        //	15 % z hrubé mzdy do 161296 Kč
        //	23 % z části hrubé mzdy přesahující 161296 Kč

        // Vypiš mzdu po odečtení této daně


    }
}
