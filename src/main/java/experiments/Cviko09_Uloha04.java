package experiments;

public class Cviko09_Uloha04
{
    public static void main(String[] args)
    {
        int[] test = {87,98,7,75,745,85786,75};
        System.out.println(count(test,75));
    }

    public static int count(int[] values, int find)
    {
        int counter = 0;
        for(int i=0; i<values.length; i++)
        {
            if(values[i] == find)
            {
                counter++;
            }
        }
        return counter;
    }
    // Vytvoř a otestuj medotu, která zjistí,
    // kolikrát se v daném poli celých čísel
    // vyskytuje daná hodnota
}
