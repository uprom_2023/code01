package experiments;

import java.util.Scanner;

public class Cviko05_Uloha05
{
    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);
        String jmeno = scanner.nextLine();
        String pozpatku = "";
        for(int i=0; i<jmeno.length(); i++)
        {
            pozpatku = jmeno.charAt(i) + pozpatku;
        }
        System.out.println(pozpatku);

        //	Vypiš zadaný řetězec pozpátku

        //  Jeden z možných postupů:
        // - vytvořit stringovou proměnnou pro výsledný řetězec, inicializovat ji prázdným stringem ("")
        // - přečíst znaky původního řetězce klasicky od prvního do posleního
        // - každý přečtený znak přidat na začátek výsledného řetězce
    }
}
