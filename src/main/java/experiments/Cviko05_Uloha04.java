package experiments;

import java.util.Scanner;

public class Cviko05_Uloha04
{
    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);
        int count = scanner.nextInt();

        String aktualniNejdelsiRetezec = "";
        for(int i=0; i<count; i++)
        {
            String s = scanner.nextLine();
            int delka = s.length();
            if(delka > aktualniNejdelsiRetezec.length())
            {
                aktualniNejdelsiRetezec = s;
            }
        }
        // Přečti z konzole "count" řetězců
        // a nakonec vypiš nejdelší z nich
    }
}
