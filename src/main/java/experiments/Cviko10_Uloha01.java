package experiments;
import java.io.FileNotFoundException;
import java.util.ArrayList;

public class Cviko10_Uloha01
{
    public static void main(String[] args) throws FileNotFoundException
    {
        // Budeme uchovávat proměnlivou posloupnost řetězců
        // pomocí pole o celkové kapacitě 1_000_000.
        // Využité pozice budou vždy v počátečním
        // úseku pole.
        // Nevyužité pozice budou vyplněné hodnotou null.
        String[] texts = new String[1_000_000];
        texts[0] = null;
        Cviko10_Utils.fillExample(texts);
        System.out.println(count(texts));
        Cviko10_Uloha03.add("Ahoj!", texts);
        System.out.println(count(texts));
    }

    // Doplň metodu, která vrátí aktuální počet řetězců
    // v posloupnosti
    public static int count(String[] texts)
    {
        for(int i =0; i<texts.length; i++)
        {
            if(texts[i]==null)
            {
                return i;
            }
        }

        return texts.length;
    }
}
