package experiments;

import java.io.FileNotFoundException;

public class Cviko10_Uloha09
{
    public static void main(String[] args) throws FileNotFoundException
    {
        String[] texts = new String[1_000_000];
        Cviko10_Utils.fillExample(texts);
        String all = "";
        // toto potrvá dlouho
        for(int i=0; i<400_000; i++ )
        {
            all = all + texts[i];
            System.out.println(i);
        }
        System.out.println(all);
    }

    // Doplň metodu, která sloučí všechny řetězce
    // v posloupnosti pomocí daného oddělovače
}
