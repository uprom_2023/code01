package experiments;

public class Cviko11_List
{
    private String[] texts = new String[1_000_000];
    private int count = 0;

    public int count()
    {
        return count;
    }

    public void add(String newValue)
    {
        if(texts[texts.length-1] != null)
        {
            return;
        }

        for(int i=0; i< texts.length; i++)
        {
            if(texts[i] == null)
            {
                texts[i] = newValue;
                break;
            }
        }

        count++;
    }

    public void swap(int index1, int index2)
    {
        if(index1 <0 || index1>= texts.length ||
                index2 <0 || index2>= texts.length ||
                texts[index1] == null || texts[index2]==null)
        {
            throw new IndexOutOfBoundsException();
        }

        String temp = texts[index1];
        texts[index1] = texts[index2];
        texts[index2] = temp;
    }

    public void remove(String value)
    {
        int foundIndex = -1;
        for(int i=0; i < texts.length; i++)
        {
            if(texts[i].equals(value))
            {
                texts[i] = null;
                foundIndex = i;
                break;
            }
        }
        if(foundIndex == -1)
        {
            return;
        }
        // Fáze2: posuň prvky vpravo doleva
        for(int rewriteIndex=foundIndex; rewriteIndex < texts.length-1; rewriteIndex++)
        {
            texts[ rewriteIndex ] = texts[rewriteIndex+1];
            if( texts[rewriteIndex+1] == null )
            {
                break;
            }
        }
        count--;
    }

    public void insert(String newValue, int index)
    {
        if(index > count || index < 0)
        {
            return;
        }
        int lastFullIndex = count-1;
        for(int i = lastFullIndex; i >= index ; i--)
        {
            texts[i+1] = texts[i];
        }
        texts[index] = newValue;
        count++;
    }

    public String join(String separator)
    {
        StringBuilder result = new StringBuilder();
        for (int i=0; i<count; i++)
        {
            if(i > 0)
                result.append(separator);
            result.append(texts[i]);
        }
        return result.toString();
    }

    // TODO 7: Přepracuj tuto třídu tak,
    //  aby byl udržován aktuální počet uložených prvků

    public String get(int index)
    {
        return texts[index];
    }

    public void set(int index, String value)
    {
        if(index <0 || index > count)
        {
            throw new IndexOutOfBoundsException();
        }
        texts[index] = value;
    }

    public String[] copyRange(int startIndex, int rangeCount)
    {
        if(startIndex <= 0 || startIndex+rangeCount > count)
        {
            throw new IndexOutOfBoundsException();
        }

        String[] result = new String[rangeCount];
        for(int i=startIndex; i<startIndex+rangeCount; i++)
        {
            result[i-startIndex] = texts[i];
        }
        // Alternativně:
        for(int i=0; i<rangeCount; i++)
        {
            result[i] = texts[startIndex+i];
        }

        return result;
    }

    public void writeRange(int startIndex, String[] values)
    {
        if(startIndex <0 || startIndex > count)
        {
            throw new IndexOutOfBoundsException();
        }
        for (int i= 0; i< values.length; i++ )
        {
            texts[startIndex+i] = values[i];
        }
    }
}
