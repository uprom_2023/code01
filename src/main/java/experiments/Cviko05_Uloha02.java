package experiments;

import java.util.Scanner;

public class Cviko05_Uloha02
{
    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);
        String jmeno = scanner.nextLine();

        for(int i=0; i<jmeno.length(); i++)
        {
            if(jmeno.charAt(i) == ' ')
            {
                System.out.println("Na indexu "+i+" je mezera.");
                System.out.println((i+1)+"-tý znak je mezera.");
            }
        }

        //	Vypiš, na kterých indexech (počítáno od 0)
        // se v zadaném jménu vyskytují mezery

        //	Vypiš, kolikáté znaky (počítáno od 1)
        // v zadaném jménu jsou mezery
    }
}
