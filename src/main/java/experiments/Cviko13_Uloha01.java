package experiments;

public class Cviko13_Uloha01
{
    //Jaká bude hodnota proměnné ahoj po provedení metody main?
    public static void main(String[] args)
    {
        // Co se vypíše?
        int ahoj = 5;
        ahoj = compute(grow(ahoj) + grow(1 + compute(ahoj)));
        System.out.println(ahoj);
    }

    public static int compute(int number)
    {
        return 2*number;
    }

    public static int grow(int number)
    {
        return number + 2;
    }

}
