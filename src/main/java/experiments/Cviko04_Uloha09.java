package experiments;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Cviko04_Uloha09
{
    public static void main(String[] args) throws IOException {
        String path = System.getProperty("user.dir")+"/sample01.txt";
        Scanner scanner = new Scanner(new File(path));

        //	Opakuj do nekonečna: přečíst řetězec (String)
        //	ze souboru sample01 a pro každý řádek vypsat,
        //  zda jde o rodné číslo muže nebo ženy (pokud je platné)
        
        while (scanner.hasNextLine())
        {
            String s = scanner.nextLine();

            // Způsob 1
            int mesic = Integer.parseInt(s.substring(2,4));
            // if(mesic >= 1 && mesic <= 12 ) { ... }

            // Způsob 2
            if(s.charAt(2) == '0' || s.charAt(2)=='1' )
            {
                System.out.println("Asi muž");
            }
            else if(s.charAt(2) == '5' || s.charAt(2)=='6')
            {
                System.out.println("Asi žena");
            }
            else
            {
                System.out.println("Neplatné číslo");
            }

        }
    }
}
