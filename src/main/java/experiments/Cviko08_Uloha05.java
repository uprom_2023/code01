package experiments;

public class Cviko08_Uloha05
{
    public static void main(String[] args)
    {
        double result = solution(2,-6,2);
        System.out.println(result);
    }

    // Tato metoda vrátí jeden kořen rovnice ax^2+bx+c=0
    // Pokud řešení neexistuje, vrátí null
    public static double solution(double a, double b, double c)
    {
        double d = b*b - 4*a*c;
        if(d < 0)
            return Double.NaN;
        double x1 = (-b+Math.sqrt(d)) / (2*a);
        return x1;
    }

}
