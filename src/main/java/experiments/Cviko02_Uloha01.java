package experiments;
import java.util.Scanner;

public class Cviko02_Uloha01
{
    public static void main(String[] args)
    {
        int f2 = 6 + 7 * 8;
        float f3 = 6 * 7 / 8;

        System.out.println(f2);


        byte b = -128;
        float f = 46.5f / 5.5f;
        double f1 = 1.234 / 5.5;

        int z1 = 59;
        z1++; // z1 = z1 + 1
        ++z1; // z1 = z1 + 1
        z1 %= 5;
        System.out.println(z1);

        int x = 48;
        int y = 5;
        System.out.println(x / y);
        System.out.println(x % y);

        final float d = 3;
        float t = 1 / d;
        System.out.println(t);
        float u =(float) Math.pow(t,16) * 43046721;
        float z = (u - 1) * 1000000;
        System.out.println(z);
    }
}
