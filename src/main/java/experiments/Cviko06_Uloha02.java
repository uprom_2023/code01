package experiments;

import java.util.Scanner;

public class Cviko06_Uloha02
{
    public static void main(String[] args)
    {
        // Načtěte přirozené číslo h. Napište kód,
        // kterým určíte nejmenší přirozené číslo m,
        // jehož druhá mocnina (m na druhou) je větší
        // než zadaná vstupní hodnota h.
        // Toto číslo m vypište.
        Scanner scanner = new Scanner(System.in);
        int h = scanner.nextInt();
        int m = 1;

        while(!(m*m > h)) // m*m <= h
        {
           m++;
        }

        System.out.println(m);

    }
}
