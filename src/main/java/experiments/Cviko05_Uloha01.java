package experiments;

import java.util.Scanner;

public class Cviko05_Uloha01
{
    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);
        String jmeno = scanner.nextLine();

        int counter = 0;
        for(int i=0; i<jmeno.length(); i++)
        {
            if(jmeno.charAt(i) == ' ')
            {
                counter = counter + 1;
                // NEBO counter+=1;
                // NEBO counter++;
            }
        }
        System.out.println(counter);
        //	Vypiš, kolik zadané jméno obsahuje mezer
    }
}
