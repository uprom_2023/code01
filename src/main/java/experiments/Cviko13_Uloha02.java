package experiments;

public class Cviko13_Uloha02
{
    //Jaká bude hodnota proměnné ahoj po provedení metody main?
    public static void main(String[] args)
    {
        // Co se vypíše?
        int[] a1 = new int[]{5,6,7};
        int r= method1(method2(a1,0), method2(a1,1));
        System.out.println(r);
    }

    public static int method2(int[] numbers, int p)
    {
        return numbers[p] + numbers[p+1];
    }

    public static int method1(int a, int b)
    {
        return a + b;
    }

}
