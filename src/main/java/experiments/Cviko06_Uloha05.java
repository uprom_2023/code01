package experiments;

import java.util.Random;

public class Cviko06_Uloha05
{
    public static void main(String[] args)
    {
        // Simuluj házení dvěma kostkami a informuj na konzoli,
        // kolikrát bylo potřeba hodit aby součet všech hodů
        // byl aspoň 1000

        Random random = new Random();
        int soucet = 0;
        int pocetVsechHodu = 0;
        while( soucet < 1000)
        {
            int x1= 1+random.nextInt(6);
            int x2= 1+random.nextInt(6);
            int obe = x1+x2;
            pocetVsechHodu++;
            soucet += obe;
        }
        System.out.println(pocetVsechHodu);
    }
}
