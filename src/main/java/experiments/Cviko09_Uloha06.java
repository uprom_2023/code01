package experiments;

import java.util.ArrayList;

public class Cviko09_Uloha06
{
    public static void main(String[] args)
    {
        double[] a = new double[]{2,3.5,4.5};
        sqrts2(a);
        System.out.println(a[0]);
    }

    public static void sqrts2(double[] values)
    {
        for(int i=0; i<values.length;i++)
        {
            values[i] = Math.sqrt(values[i]);
        }
    }

    public static double[] sqrts(double[] input)
    {
        double[] result = new double[input.length];
        for(int i=0; i<result.length;i++)
        {
            result[i] = Math.sqrt(input[i]);
        }
        return result;
    }
}
