package experiments;
import java.io.FileNotFoundException;

public class Cviko12_Uloha02
{
    // Příprava na test

    public static void main(String[] args) throws FileNotFoundException
    {
        /*░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
        Doplň kód, který:
         - založí dvě pole typu int[], každé o délce 5
         - použije tyto pole jako vstup do metody concatenate
           a vypíše délku pole, které se vrátilo jako výstup
        ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░*/
    }

    public static int[] concatenate(int[] values1, int[] values2)
    {

        /*░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
        Doplň kód tak, aby metoda vrátila pole, ve kterém jsou
        zkopírovány nejdříve všechny prvky pole values1 a potom všechny prvky
        pole values2
        ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░*/

        return null;
    }
}
