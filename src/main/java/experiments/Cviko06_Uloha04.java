package experiments;

import java.util.Random;

public class Cviko06_Uloha04
{
    public static void main(String[] args)
    {
        // Simuluj házení dvěma kostkami a informuj na konzoli,
        // kolikrát bylo potřeba hodit aby desetkrát padl součet 7

        Random random = new Random();

        int pocetVsechHodu = 0;
        int kolikratPadlo7 = 0;
        while(kolikratPadlo7 < 10)
        {
            int x1= 1+random.nextInt(6);
            int x2= 1+random.nextInt(6);
            int obe = x1+x2;
            pocetVsechHodu++;
            if(obe==7)
            {
                kolikratPadlo7++;
            }
        }
        System.out.println(pocetVsechHodu);
    }
}
