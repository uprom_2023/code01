package experiments;

public class Cviko09_Uloha03
{
    public static void main(String[] args)
    {
        int[] test = {87,98,7,75,745,85786};
        System.out.println(isAscending(test));
    }

    public static boolean isAscending(int[] values)
    {
        boolean result = true;
        for(int i=0; i<values.length-1; i++)
        {
            if(values[i] > values[i+1])
            {
                result = false;
                break;
            }
        }
        return result;
    }
    // Vytvoř a otestuj medotu, která otestuje,
    // zda dané pole
    // celých čísel je vzestupně seřazené

}
