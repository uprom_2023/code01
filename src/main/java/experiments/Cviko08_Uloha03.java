package experiments;

public class Cviko08_Uloha03
{
    public static void main(String[] args)
    {
        boolean result = isLeapYear(2055);
        if(result)
        {
            System.out.println("Je přestupný");
        }
        else
        {
            System.out.println("Není přestupný");
        }
    }

    public static boolean isLeapYear(int rok)
    {
        if(rok % 4 != 0)
        {
            return false;
        }
        else
        {
            if(rok % 100 != 0)
            {
                return true;
            }
            else
            {
                if(rok % 400 != 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
    }
}
