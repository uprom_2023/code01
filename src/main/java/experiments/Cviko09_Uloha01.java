package experiments;

import java.util.Scanner;

public class Cviko09_Uloha01
{
    public static void main(String[] args)
    {
        System.out.println("Zadej deset hodnot:");
        Scanner scanner = new Scanner(System.in);
        int[] mojePole = new int[10];
        for(int i=0; i<10; i++)
        {
            mojePole[i] = scanner.nextInt();
        }

        System.out.println("Vaše hodnoty v opačném pořadí:");
        for(int i=0; i<10; i++)
        {
            System.out.println(mojePole[9-i]);
        }
    }
}
