package experiments;

import java.io.FileNotFoundException;

public class Cviko10_Uloha07
{
    public static void main(String[] args) throws FileNotFoundException
    {
        String[] texts = new String[1_000_000];
        Cviko10_Utils.fillExample(texts);
        System.out.println(texts[0]);
        System.out.println(texts[1]);
        System.out.println(texts[2]);
        System.out.println(texts[3]);
        System.out.println(texts[4]);
        remove("abactinally",texts);
        System.out.println();
        System.out.println(texts[0]);
        System.out.println(texts[1]);
        System.out.println(texts[2]);
        System.out.println(texts[3]);
        System.out.println(texts[4]);
    }

    // Doplň metodu, která odstraní z posloupnosti
    // jeden výskyt daného řetězce
    public static void remove(String value, String[] texts)
    {
         // Fáze1: najdi index, kde to je
        int foundIndex = -1;
        for(int i=0; i < texts.length; i++)
        {
            if(texts[i].equals(value))
            {
                texts[i] = null;
                foundIndex = i;
                break;
            }
        }
        if(foundIndex == -1)
        {
            return;
        }
        // Fáze2: posuň prvky vpravo doleva
        for(int rewriteIndex=foundIndex; rewriteIndex < texts.length-1; rewriteIndex++)
        {
            texts[ rewriteIndex ] = texts[rewriteIndex+1];
            if( texts[rewriteIndex+1] == null )
            {
                break;
            }
        }

    }
}
