package experiments;

import java.util.Scanner;

public class Cviko04_Uloha06
{
    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);

        //	Opakuj do nekonečna: přečíst řetězec (String) z konzole
        //  a vypsat "OK" pokud má 11 znaků, sedmým znakem je lomítko
        //  a ostatní znaky jsou číslice. Pokud to tak není,
        //  vypsat v čem je problém.
        String s = scanner.nextLine();
        System.out.println(s.length());
        boolean problemFound = false;
        if(s.length() != 11)
        {
            System.out.println("Špatná délka!");
            problemFound = true;
        }
        if(!(s.charAt(6) == '/'))
        {
            System.out.println("Chybí lomítko!");
            problemFound = true;
        }

        for(int i =0; i < s.length(); i++)
        {
            // i nabyde postupně hodnot 0,1,2,...,"LENGTH-1"
            char znak = s.charAt(i);
            if(i != 6)
            {
                if (znak < '0' || znak > '9')
                {
                    System.out.println("Znak není číslice!");
                    problemFound = true;
                }
            }
        }

        if(!problemFound)
        {
            System.out.println("OK");
        }

    }
}
