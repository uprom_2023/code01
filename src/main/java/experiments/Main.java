package experiments;

import java.util.Scanner;

public class Main
{
    public static void main(String[] args)
    {

        Scanner scanner = new Scanner(System.in);
        int h=scanner.nextInt();
        int sum=0;
        for(int i=0;i<h;i++)
        {
            for(int j=i;j>0;j-=1)
            {
                sum +=j;
            }
            if(sum > h){
                System.out.println(i);
                return;
            }
            else {
                sum=0;
            }

        }
    }
}
