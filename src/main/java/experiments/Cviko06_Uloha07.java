package experiments;

import java.util.Scanner;

public class Cviko06_Uloha07
{
    public static void main(String[] args)
    {
        // Z konzole načti přirozené číslo N
        // Opakuj následující postup dokud N nebude rovné 1:
        // - pokud je sudé, vyděl N dvěma
        // - pokud je liché, vynásob N třemi a přičti 1
        // (viz https://en.wikipedia.org/wiki/Collatz_conjecture)

        // Stačí upravovat pořád tutéž proměnnou N!

        Scanner scanner = new Scanner(System.in);
        long N = scanner.nextLong();

        while (N != 1)
        {
            if(N % 2 == 0)
            {
                N = N / 2;
            }
            else
            {
                N = N * 3 +1;
            }
            System.out.println(N);
        }

    }
}
