package experiments;

import java.io.FileNotFoundException;
import java.text.Normalizer;

public class Cviko10_Uloha03
{
    public static void main(String[] args) throws FileNotFoundException
    {
        String[] texts = new String[1_000_000];
        Cviko10_Utils.fillExample(texts);
        System.out.println(texts[0]);
        System.out.println(texts[1]);
        System.out.println(texts[2]);
        System.out.println(texts[3]);
        System.out.println(texts[4]);
        add("Ahoj!", texts);
    }

    // Pokud je plno, neudělá nic
    public static void add(String newValue, String[] texts)
    {
        if(texts[texts.length-1] != null)
        {
            return;
        }

        for(int i=0; i< texts.length; i++)
        {
            if(texts[i] == null)
            {
                texts[i] = newValue;
                break;
            }
        }
    }
}
