package experiments;

public class Cviko09_Uloha02
{
    public static void main(String[] args)
    {
        int[] interval = getInterval(200);
        for(int i=0; i<interval.length; i++)
        {
            System.out.println(interval[i]);
        }
    }

    // Doplň a otestuj medotu, která vrátí pole
    // po sobě jdoucích celých čísel, počínaje nulou
    public static int[] getInterval(int count)
    {
        int[] result = new int[count];
        for(int i=0; i<result.length; i++)
        {
            result[i] = i;
        }
        return result;
    }

}
