package experiments;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Cviko04_Uloha08
{
    public static void main(String[] args) throws IOException {
        String path = System.getProperty("user.dir")+"/sample01.txt";
        Scanner scanner = new Scanner(new File(path));

        //	Opakuj do nekonečna: přečíst řetězec (String)
        //	ze souboru sample01 a  zpracovat řádky stejně jako
        //  v úloze 06

        while (scanner.hasNextLine())
        {
            String s = scanner.nextLine();
            System.out.println(s.length());
            boolean problemFound = false;
            if(s.length() != 11)
            {
                System.out.println("Špatná délka!");
                problemFound = true;
            }
            if(!(s.charAt(6) == '/'))
            {
                System.out.println("Chybí lomítko!");
                problemFound = true;
            }

            for(int i =0; i < s.length(); i++)
            {
                // i nabyde postupně hodnot 0,1,2,...,"LENGTH-1"
                char znak = s.charAt(i);
                if(i != 6)
                {
                    if (znak < '0' || znak > '9')
                    {
                        System.out.println("Znak není číslice!");
                        problemFound = true;
                    }
                }
            }

            if(!problemFound)
            {
                System.out.println("OK");
            }

        }
    }
}
