package experiments;

import java.io.FileNotFoundException;

public class Cviko10_Uloha05
{
    public static void main(String[] args) throws FileNotFoundException
    {
        String[] texts = new String[1_000_000];
        Cviko10_Utils.fillExample(texts);
        System.out.println(texts[0]);
        System.out.println(texts[1]);
        System.out.println(texts[2]);
        System.out.println(texts[3]);
        System.out.println(texts[4]);
        swap(1,4,texts);
        System.out.println();
        System.out.println(texts[0]);
        System.out.println(texts[1]);
        System.out.println(texts[2]);
        System.out.println(texts[3]);
        System.out.println(texts[4]);
    }

    public static void swap(int index1, int index2, String[] texts)
    {
        if(index1 <0 || index1>= texts.length ||
                index2 <0 || index2>= texts.length ||
                texts[index1] == null || texts[index2]==null)
        {
            throw new IndexOutOfBoundsException();
        }

        String temp = texts[index1];
        texts[index1] = texts[index2];
        texts[index2] = temp;
    }

    // Doplň metodu, která přehodí řezězce na dvou
    // daných pozicích v posloupnosti

}
