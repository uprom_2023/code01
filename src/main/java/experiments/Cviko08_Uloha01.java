package experiments;

public class Cviko08_Uloha01
{
    //V metodě main volej metodu addHalf a vypiš výsledek.

    public static void main(String[] args)
    {
        float mainA = 648.4f;
        float mainB = 452.1f;
        float result = addHalf(mainA, mainB);
        System.out.println(result);
    }

    public static float addHalf(float a, float b)
    {
        return a + b/2;
    }
}
