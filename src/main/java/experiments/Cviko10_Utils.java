package experiments;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Cviko10_Utils
{
    public static void fillExample(String[] texts) throws FileNotFoundException
    {
        String path = System.getProperty("user.dir")+"/sample02.txt";
        Scanner scanner = new Scanner(new File(path));
        int i=-1;
        while (scanner.hasNextLine())
        {
            i++;
            texts[i] = scanner.nextLine();
        }
    }
}
