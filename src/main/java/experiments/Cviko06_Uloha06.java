package experiments;

import java.util.Scanner;

public class Cviko06_Uloha06
{
    public static void main(String[] args)
    {
        // Z konzole načti přirozené číslo N
        // Vypiš všechna Fibonacciho čísla nižší než N

        // Použít dvě proměnné: posledniPrvek, predposledniPrvek
        // Pokud posledniPrvek > N, končíme

        // Příklad: Vstup: 50
        // Výstup (řádky): 1, 1, 2, 3, 5, 8, 13, 21, 34

        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt();
        int posledniPrvek = 1;
        int predposledniPrvek = 1;
        while(posledniPrvek <= N)
        {
            System.out.println(posledniPrvek);
            int temp = predposledniPrvek;
            predposledniPrvek = posledniPrvek;
            posledniPrvek = temp + posledniPrvek;
        }
    }
}
