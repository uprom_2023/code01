package experiments;

public class Cviko08_Uloha04
{
    public static void main(String[] args)
    {
        boolean result = isMuchLowerThan(3548,68766);
    }

    // Tato metoda zjišťuje jestli A je méně než setinou čísla B
    public static boolean isMuchLowerThan(double a, double b)
    {
        return a < b/100;
    }
}
