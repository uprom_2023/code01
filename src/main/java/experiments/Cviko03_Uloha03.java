package experiments;

import java.util.Scanner;

public class Cviko03_Uloha03
{
    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);
        double hrubaMzda = scanner.nextDouble();

        boolean ztp = true;
        boolean student = true;
        int pocetDeti = 2;
        // Vypiš čistou mzdu podle postupu uvedeného v zápiscích
        // KROK 2
        double danZPrijmu =0;
        if(hrubaMzda < 161296)
        {
            danZPrijmu = hrubaMzda * 0.15;
        }
        else
        {
            danZPrijmu = 161296 * 0.15 + (hrubaMzda - 161296) * 0.23;
        }

        // KROK 3
        danZPrijmu = danZPrijmu - 2570;
        if(ztp)
        {
            danZPrijmu = danZPrijmu - 1345;
        }
        if(student)
        {
            danZPrijmu = danZPrijmu - 335;
        }

        if(danZPrijmu < 0)
        {
            danZPrijmu = 0;
        }

        // KROK 4
        if(pocetDeti >= 1)
        {
            danZPrijmu = danZPrijmu - 1267;
        }
        if(pocetDeti >= 2)
        {
            danZPrijmu = danZPrijmu - 1860;
        }
        if(pocetDeti >= 3)
        {
            danZPrijmu = 2320 * (pocetDeti-2);
        }

        // KROK 5
        if(danZPrijmu < 0)
        {
            if(hrubaMzda < 8650)
            {
                danZPrijmu = 0;
            }
            if(danZPrijmu > -100)
            {
                danZPrijmu = 0;
            }
            if(danZPrijmu < -73200)
            {
                danZPrijmu = -73200;
            }
        }

        // KROK 6
        double zdravotniPojisteni = hrubaMzda * 0.045;
        double socialniPojisteni = hrubaMzda * 0.065;
        double cistaMzda
                = hrubaMzda - danZPrijmu - zdravotniPojisteni - socialniPojisteni;
        System.out.println(cistaMzda);
    }
}
