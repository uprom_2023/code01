package experiments;
import java.util.Scanner;

public class Cviko01_Uloha01
{
    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);
        float sum = 0;
        float newValue = 0;
        int count = 0;
        float result = 0;
        System.out.println("Zadej čísla, ukonči nulou:");
        while(true)
        {
            newValue = scanner.nextFloat();
            if(newValue == 0)
            {
                break;
            }
            sum = sum + newValue;
            count = count + 1;
        }
        result = sum / count;
        System.out.println("Výsledek je:");
        System.out.println(result);
    }
}
