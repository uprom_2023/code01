package experiments;

import java.util.Scanner;

public class Cviko06_Uloha03
{
    public static void main(String[] args)
    {
        // Načtěte přirozené číslo h.
        // Napište kód, kterým vypíšete všechna přirozená čísla m,
        // která jsou menší než h a jsou sudá.
        Scanner scanner = new Scanner(System.in);
        int h = scanner.nextInt();
        for( int i=0  ; i < h ;  i++ )
        {
            if(i % 2 == 0) // je i sudé?
            {
                System.out.println(i);
            }
        }
    }
}
