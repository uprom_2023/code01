package experiments;

import java.util.Random;

public class Cviko09_Uloha05
{
    public static void main(String[] args) {
        float[] p= new float[]{1,2,-5,-51,-5,5,-5,-6,};
        System.out.println(longestNegativeInterval(p));

        Random random = new Random();
        float[] tempertures = new float[10_000_000];
        for (int i = 0; i < tempertures.length; i++)
        {
            tempertures[i]= random.nextFloat()*100-50;
        }
        System.out.println(longestNegativeInterval(tempertures));
    }

    public static int longestNegativeInterval(float[] values)
    {
        int counter = 0;
        int max = 0;
        for(int i=0; i<values.length; i++)
        {
            if(values[i] < 0)
            {
                counter++;
                if(counter > max)
                {
                    max = counter;
                }
            }
            else
            {
                counter = 0;
            }
        }
        return max;
    }


    // Vytvoř a otestuj medotu, která pro danou časovou řadu teplot
    // (reálná čísla, jedno měření za sekundu) zjistí,
    // kolik sekund trval nejdelší interval, kdy byla teplota pod nulou
}
