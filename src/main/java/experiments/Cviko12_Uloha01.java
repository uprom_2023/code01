package experiments;
import java.io.FileNotFoundException;

public class Cviko12_Uloha01
{
    // Příprava na test

    public static void main(String[] args) throws FileNotFoundException
    {
        int[] test = new int[]{68,67,98,67,387,397,63,698,988,397,322,340,570,878};
        int[] test2 = {68,67,98,67,387,397,63,698,988,397,322,340,570,878};

        /*░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
        Doplň kód, který zavolá metodu evenValuesOnOddIndexes a vypíše délku pole,
        které bylo z metody vráceno jako výstup
        ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░*/
        System.out.println(evenValuesOnOddIndexes(test).length);
    }

    public static int[] evenValuesOnOddIndexes(int[] values)
    {
        int resultCount = 0;

        /*░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
        Doplň kód, který spočítá, kolik prvků ve vstupním poli
        na lichých indexech má sudou hodnotu
        Tento počet ulož do proměnné resultCount
        ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░*/

        for(int i =0; i <values.length; i++)
        {
            if( i % 2 == 1 && values[i] % 2 == 0 )
            {
                resultCount++;
            }
        }

        int[] result = new int[resultCount];
        /*░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
        Založ správně pole result a naplň ho čísly, které
        ležely na lichých indexech a měly sudou hodnotu
        ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░*/

        int count=0;
        for(int i =0; i <values.length; i++)
        {
            if( i % 2 == 1 && values[i] % 2 == 0 )
            {
                result[count] = values[i];
                count++;
            }
        }

        return result;
    }
}
