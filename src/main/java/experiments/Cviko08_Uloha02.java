package experiments;

public class Cviko08_Uloha02
{
    public static void main(String[] args)
    {
        int result = min(3544,6868);
        System.out.println(result);
    }

    // Oprav metodu min, aby vracela menší ze dvou čísel
    // V metodě main volej metodu min a vypiš vysledek.
    public static int min(int a, int b)
    {
        if(a < b)
        {
            return a;
        }
        return b;
    }

}
